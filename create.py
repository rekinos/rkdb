#!/usr/bin/env python3

import argparse
import os
import sqlite3
import sys

parser = argparse.ArgumentParser()
parser.add_argument("filename")

args = parser.parse_args()

filename = args.filename

if os.path.exists(filename):
    sys.exit("Error: File already exists")

try:
    conn = sqlite3.connect(filename)
    try:
        c = conn.cursor()

        c.execute("""
        CREATE TABLE meta (
            version INTEGER
        )
        """)

        c.execute("INSERT INTO meta (version) VALUES (2)")

        c.execute("""
        CREATE TABLE package (
            path    TEXT PRIMARY KEY,
            type    TEXT NOT NULL,
            name    TEXT NOT NULL,
            version TEXT NOT NULL,
            arch    TEXT,
            description TEXT NOT NULL,
            size_installed  INTEGER,
            size_download   INTEGER,
            hash_sha256 TEXT
        )
        """)

        c.execute("""
        CREATE TABLE package_dependency (
            package    TEXT NOT NULL REFERENCES package (path) ON DELETE CASCADE,
            dependency    TEXT NOT NULL,
            type    TEXT NOT NULL,
            description TEXT
        )
        """)

        c.execute("""
        CREATE TABLE package_provides (
            package TEXT NOT NULL REFERENCES package (path) ON DELETE CASCADE,
            provides    TEXT NOT NULL
        )
        """)

        c.close()

        conn.commit()
    finally:
        conn.close()
except:
    os.remove(filename)
    raise
