#!/usr/bin/env python3

import argparse
import os
import sqlite3
import sys
import tarfile

def hash_file(path):
    import hashlib

    hasher = hashlib.sha256()
    with open(path, "rb") as h:
        for chunk in h:
            hasher.update(chunk)
    return hasher.hexdigest()

def parse_optdepend(value):
    info = {"type": "optional"}
    cidx = value.find(":")
    if cidx < 0:
        info["name"] = value
        info["description"] = None
    else:
        info["name"] = value[:cidx]
        info["description"] = value[(cidx + 1):].strip()
    return info

parser = argparse.ArgumentParser()
parser.add_argument("db_path")
parser.add_argument("package", nargs="+")

args = parser.parse_args()

db_path = args.db_path
package_files = args.package

if not os.path.exists(db_path):
    sys.exit("Error: DB not found")

conn = sqlite3.connect(db_path)
try:
    c = conn.cursor()

    c.execute("SELECT version FROM meta")

    metarow = c.fetchone()

    VERSION = 2
    if metarow[0] != VERSION:
        raise RuntimeError("Unsupported DB version: %s, expected %s" % (metarow[0], VERSION))

    def add_dependencies(package_path, pkg_dependencies):
        for dep in pkg_dependencies:
            c.execute(
                "INSERT INTO package_dependency (package, dependency, type, description) VALUES (?, ?, ?, ?)",
                (package_path, dep["name"], dep["type"], dep["description"])
            )

    def add_provides(package_path, pkg_provides):
        for provides in pkg_provides:
            c.execute("INSERT INTO package_provides (package, provides) VALUES (?, ?)", (package_path, provides))

    def add_binary_package(package_path, entry):
        filesize = os.stat(package_path).st_size
        filehash = hash_file(package_path)

        pkg_name = None
        pkg_version = None
        pkg_description = None
        pkg_size_installed = None
        pkg_arch = None
        pkg_dependencies = []
        pkg_provides = []

        lines = entry.readlines()
        for line in lines:
            line = line.split(b"#")[0].decode("utf-8").strip()
            if not line:
                continue
            idx = line.find("=")
            if idx < 0:
                raise RuntimeError("Invalid line found in PKGINFO: " + line)
            
            key = line[0:idx].strip()
            value = line[(idx + 1):].strip()

            if key == "pkgname":
                pkg_name = value
            elif key == "pkgver":
                pkg_version = value
            elif key == "pkgdesc":
                pkg_description = value
            elif key == "size":
                pkg_size_installed = value
            elif key == "arch":
                pkg_arch = value
            elif key == "depend":
                pkg_dependencies.append({
                    "type": "all",
                    "name": value,
                    "description": None,
                })
            elif key == "optdepend":
                pkg_dependencies.append(parse_optdepend(value))
            elif key == "makedepend":
                pkg_dependencies.append({
                    "type": "make",
                    "name": value,
                    "description": None,
                })
            elif key == "provides":
                pkg_provides.append(value)

        c.execute("INSERT INTO package (path, type, name, version, arch, description, size_installed, size_download, hash_sha256) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)", (package_path, "binary", pkg_name, pkg_version, pkg_arch, pkg_description, pkg_size_installed, filesize, filehash))

        add_dependencies(package_path, pkg_dependencies)
        add_provides(package_path, pkg_provides)

    def add_source_package(package_path, entry):
        pkg_name = None
        pkg_version_base = None
        pkg_version_rel = None
        pkg_description = None
        pkg_arch = None
        pkg_dependencies = []
        pkg_provides = []

        lines = entry.readlines()

        for line in lines:
            line = line.split(b"#")[0].decode("utf-8").strip()
            if not line:
                continue
            idx = line.find("=")
            if idx < 0:
                raise RuntimeError("Invalid line found in SRCINFO: " + line)

            key = line[0:idx].strip()
            value = line[(idx + 1):].strip()

            if key == "pkgdesc":
                pkg_description = value
            elif key == "pkgver":
                pkg_version_base = value
            elif key == "pkgrel":
                pkg_version_rel = value
            elif key == "arch":
                pkg_arch = value
            elif key == "depends":
                pkg_dependencies.append({
                    "type": "all",
                    "name": value,
                    "description": None,
                })
            elif key == "optdepends":
                pkg_dependencies.append(parse_optdepend(value))
            elif key == "makedepends":
                pkg_dependencies.append({
                    "type": "make",
                    "name": value,
                    "description": None,
                })
            elif key == "provides":
                pkg_provides.append(value)
            elif key == "pkgname":
                if pkg_name is not None:
                    raise RuntimeError("Split packages are not supported.")
                pkg_name = value

        c.execute("INSERT INTO package (path, type, name, version, arch, description) VALUES (?, ?, ?, ?, ?, ?)", (package_path, "source", pkg_name, pkg_version_base + "-" + pkg_version_rel, pkg_arch, pkg_description))

        add_dependencies(package_path, pkg_dependencies)
        add_provides(package_path, pkg_provides)

    for package_path in package_files:
        archive = tarfile.open(package_path, 'r')
        binentry = None
        try:
            binentry = archive.extractfile(".PKGINFO")
        except KeyError:
            pass

        if binentry is not None:
            add_binary_package(package_path, binentry)
        else:
            for member in archive.getmembers():
                if member.name.endswith("/.SRCINFO"):
                    add_source_package(package_path, archive.extractfile(member))
    conn.commit()
finally:
    conn.close()
